ENV=source .env/bin/activate

all: type
	$(ENV) && echo "" && python rewrite.py fun_test/project.json

run: all
	python out.py

debug: type
	$(ENV) && echo "" && python rewrite.py --debug fun_test/project.json

type: rewrite.py arghandling.py
	$(ENV) && python -m mypy rewrite.py
