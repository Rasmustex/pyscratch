from sys import argv, stderr, stdout
from typing import Tuple, Any
import json as js
from arghandling import CmdParser, print_help

DEBUG = False

def main():
    global DEBUG

    Parser = CmdParser()
    Parser.create_and_add_arg("-h", "--help", bool, False, "Print help menu and exit")
    Parser.create_and_add_arg("-d", "--debug", bool, False, "Run in debug mode")
    Parser.create_and_add_arg("-o", "--output", str, "out.py", "Specify path to output file")
    progname, source_file = handle_args(argv, Parser)

    output = Parser.get_option_value("output")
    DEBUG  = Parser.get_option_value("debug")

    source = parse_scratch_json(source_file)
    if DEBUG:
        output_file = stdout
    else:
        output_file = open(output, "w")

    translate_to_file(source, f=output_file)
    if not DEBUG:
        output_file.close()

def handle_args(args: list[str], Parser: CmdParser) -> Tuple[str, str]:
    progname, args = Parser.parse_args(args)

    if Parser.get_option_value("help"):
        print_help(progname, usage_string_set=f"[FILE] ... [OPTIONS]\nTranspile scratch project.json FILE to python code")
        exit(0)

    if len(args) == 0:
        print_help(progname)
        print("ERROR: no filename provided", file=stderr)
        exit(1)
    elif len(args) > 1:
        print_help(progname)
        print(f"ERROR: more than one input file name provided: {args}", file=stderr)
        exit(1)

    source_file = args[0]
    return progname, source_file

def parse_scratch_json(source_file) -> dict:
    with open(source_file, "r") as f:
        prog_str = f.read()
        program = js.loads(prog_str)
    return program["targets"][0]

def translate_block(block: dict) -> str:
    match block["opcode"]:
        case "data_setvariableto":
            operand = block["fields"]["VARIABLE"][0].replace(" ", "_")
            arg = block["inputs"]["VALUE"][-1][-1]
            return f"{operand} = {arg}"

        case "procedures_call":
            #TODO: multiple arguments
            arg = block["inputs"][block["mutation"]["argumentids"][2:-2]][1][1]
            proccode = block["mutation"]["proccode"]

            if proccode[:9] == "BUILTIN: ":
                proccode = proccode[9:].split(" ")[0]
                return f"{proccode}({arg})"
            else:
                raise NotImplementedError(f"Block {block}: Translation for custom function calls not yet implemented")
        case x:
            raise NotImplementedError(f"Block {block}: Translation for opcode {x} not yet implemented")

def write_program(vars: list[Tuple[int, int]], proc_dict: dict[str, Any], f) -> None:
    # variables
    f.write("#### Variables\n")
    for var in vars:
        f.write(f"{var[0]} = {var[1]}\n")

    f.write("\n")

    indent_level = 0
    def prindent(s: str):
        f.write(indent_level * "    " + s)

    # procedures
    for proc_key in proc_dict:
        f.write("#### Function definition\n")

        proc = proc_dict[proc_key]
        if proc[proc_key]["opcode"] == "event_whenflagclicked":
            f.write("def main():\n")
        else:
            assert False, "procedures not yet implemented"

        indent_level += 1

        prindent("#### Global variables\n")
        for var in vars:
            prindent(f"global {var[0]}\n")

        f.write("\n")
        cbkey = proc_key
        cblock = proc[cbkey]
        while cblock["next"] is not None:
            cbkey = cblock["next"]
            cblock = proc[cbkey]
            prindent(f"{translate_block(cblock)}\n")

        indent_level -= 1

    f.write("if __name__ == \"__main__\":\n    main()\n")

# maybe have a list that is appended to, or do a list comprehension
def translate_to_file(source: dict[str, Any], f=stdout) -> None:
    if DEBUG:
        print(source)
    # retrieve list of variables with initial values
    vars = [(name.replace(" ", "_"), val)
            for name, val in (source["variables"][key]
                              for key in source["variables"])]

    proc_dict = {}
    # get program procedures
    blocks = source["blocks"]
    for bkey in blocks:
        block = blocks[bkey]
        if block["parent"] is None:
            if block["opcode"] == "event_whenflagclicked":
                cbkey = bkey
                cblock = block
                proc = {cbkey : cblock}
                while cblock["next"] is not None:
                    cbkey = cblock["next"]
                    cblock = blocks[cbkey]

                    proc[cbkey] = cblock

                proc_dict[bkey] = proc
            elif block["opcode"] == "procedures_definition":
                proccode = blocks[block["inputs"]["custom_block"][1]]["mutation"]["proccode"]
                if proccode[:9] != "BUILTIN: ":
                    raise NotImplementedError(f"Proccode {proccode}: Non-builtin function definitions aren't supported")

    write_program(vars, proc_dict, f)

if __name__ == "__main__":
    main()
